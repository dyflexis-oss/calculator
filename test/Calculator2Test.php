<?php

namespace Dyflexis;

use PHPUnit\Framework\TestCase;

/**
 * @group phase2
 */
class Calculator2Test extends TestCase
{
    public function testPow()
    {
        $calc = new Calculator();
        $this->assertSame(4, $calc->pow(2, 2));
    }

    public function testIsDividableBy()
    {
        $calc = new Calculator();
        $this->assertTrue($calc->isDividableBy(10, 2));
        $this->assertFalse($calc->isDividableBy(10, 3));
    }
}
