<?php

namespace Dyflexis;

class Calculator
{
    public function multiply($a, $b)
    {
        return $a * $b;
    }

    public function divide($a, $b)
    {
        return $a / $b;
    }
}
